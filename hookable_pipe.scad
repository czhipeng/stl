use <hook4.scad>;
use <hook5.scad>;

radius_out = 34.22/2;
height = 5;
thinkness = 2;
angle = 30;

hook_height = 9;

module hookSet() {
    rotate([0,0,0]) 
        translate([0,0,height - (2/3)*hook_height]) hook(aH = hook_height);
    rotate([0,0,180]) 
        translate([0,0,height - (2/3)*hook_height]) hook(aH = hook_height);
}

module hacker() {    
    difference() {
        union () {
            hookSet();
            translate([0,0, -1]) pipe(aR = radius_out,
            aH = height,
            aT = thinkness,
            aA = angle);
        }
        translate([0,0, 4])
            #pipe(aR = radius_out - 0.3, aH = 5, aT = 2);
    }
}

module main() {
    translate([0,0,40]) hacker();
    translate([0,0,0])
        rotate([180,0,45])
            hacker();    
}

main();
