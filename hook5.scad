use <hook4.scad>;

hook();

module hook() {
    left();
    right();
}

module left() {
    rotate([0,0,-45]) mirror([-1,-1,0]) right();
}

module right() {
    difference() {
        pipeFan(aR = (34.22/2) - 2, aH = 10, aT = 2, $fn = 100, aA = 45);
        translate([0,0,1]) {   
            rotate([-90,0,-10]) {
                cylinder(r = 5, h = 50, $fn = 100, center = true);
            }
        }
        rotate([0,0,-10]) cube([10,100,10], center = false);
    }
    
    intersection() {
        pipeFan(aR = (34.22/2) - 2, aH = 10, aT = 2, $fn = 100, aA = 45);
        translate([0,0,8]) {   
            rotate([-90,0,-10]) {
                cylinder(r = 2, h = 50, $fn = 100, center = true);
            }
        }
    }    
}

