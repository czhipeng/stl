
use <hook4.scad>;
use <hook5.scad>;

radius_out = 34.22/2;
height = 5;
thinkness = 2;
angle = 30;
web_pipe_height = 7;
hook_height = 9;

module hookSet() {
    rotate([0,0,0]) 
        translate([0,0,height - (2/3)*hook_height]) hook(aH = hook_height);
    rotate([0,0,180]) 
        translate([0,0,height - (2/3)*hook_height]) hook(aH = hook_height);
}

module hacker() {    
    difference() {
        union () {
            hookSet();
            translate([0,0, -1]) pipe(aR = radius_out,
            aH = height,
            aT = thinkness,
            aA = angle);
        }
        translate([0,0, 4])
            pipe(aR = radius_out - 0.3, aH = 5, aT = 2);
    }
}

module webPipe() {
    pipe(aR = radius_out, aH = web_pipe_height, aT = thinkness);

    intersection() {
        translate([0,0, web_pipe_height/2])
            cylinder(
                r = radius_out,
                h = web_pipe_height,
                $fn = 100,
                center = true
            );
            for (i = [0:6]) {
                translate([0, i * 5 - radius_out,  web_pipe_height/2])
                    cube([40,3,3], center = true);
            }
    }
}

module main() {
    rotate([0,0,-25]) translate([0,0,web_pipe_height]) hacker();
    webPipe();
    rotate([0,180,20]) translate([0,0,0]) hacker();
}

main();