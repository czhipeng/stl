u = 3;
thinkness = 2;
radius_out = 34.22/2 - thinkness;
angle_total = 30;

//aR 扇形角度 
//aH 扇形高度
//aT 管的厚度
//aA 管扇形角度
module pipeFan(aR, aH, aT, aA) {    
    difference() {
        pipe(aR, aH, aT);
        fan(aA);
    }
}

//aA [0,180]
module fan(aA) {
    aA2 = aA/2;
    height = 100;    
    translate([0,0,-2]) union() {
        rotate([0,0,90 + aA2]) cube([100,100, height]);
        rotate([0,0,-aA2]) cube([100,100, height]);        
        rotate([0,0,270]) translate([0,-100,0]) cube([200,200, height]);        
    }
}

module pipe(aR, aH, aT) {
    count = 200;
    difference() {
        cylinder(r = aR, h = aH, $fn = count);
        translate([0,0,-1]) cylinder(r = aR - aT, h = aH + 2, $fn = count);
    }
}

module platform() {
    platform_angle = 5;
    
    translate([-u/2,0,u/2])
        rotate([90,0,0]) 
            linear_extrude(height = 4)    
                polygon(points = [
                    [0,0],[u,u/tan(90-platform_angle)],
                    [u,-(u+u/tan(90-platform_angle))],[0,-u]
                ]);

}

module left() {
    rotate([0,0,-(angle_total/3+2)], center = true) translate([0,radius_out+0.5,u*1.5]) platform();
}

module right() {
    mirror([1,0,0]) {left();};
}

module hook(
    aH = u * 3, 
    aA = angle_total,
    aT = thinkness,
    aR = radius_out,
    aU = u) {
        
    difference() {
        difference() {
            pipeFan(aR, aH, aT, aA);
            left();
            right();
        };
        translate([0, 0 ,aU * 2]) {
            pipe(aR + 1.8, aU * 1.2, aT);        
        }
    }
    
}

hook();
//rotate(21,0,0) translate([0,0,u+0.2]) hook();


