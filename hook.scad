//单元
us = 3;

//hookOpm(34.22/2, us * 3, 2, 30, 0.3);
//
//module hookOpm(aR, aH, aT, aA, aL) {
//    difference() {
//        hook(aR, aH, aT, aA, aL);
//        translate([0,0,us*2]) pipeFan(aR+aT-0.5, us, aT, aA * 1.5);
//    }
//}

//module vn(aL, aT) {
//    intersection() {
//        rotate([0,4,0]) cube([aL, aL, aT], center = true);
//        rotate([0,-4,0]) cube([aL, aL, aT], center = true);        
//    }
//}

//module clipPipe(aR, aH, aT, aA, aL) {
//    intersection() {
//        pipeFan(aR+1, aH/3, aT+3, aA);       
//        translate([0,0,aH/3/2]) vn(50, 2);        
//    }
//}

////aL剪剩的百分比
//module hook(aR, aH, aT, aA, aL) {
//    difference() {
//        pipeFan(aR, aH, aT, aA);
//        //剪去右边
//            translate([0, 0, us]) 
//                rotate([0, 0, -aA*(1-aL)])
//                    clipPipe(aR, aH, aT, aA, aL);
//        //剪去左边
//        color([1,0,0])
//            translate([0, 0, us])
//                rotate([0, 0, aA*(1-aL)]) 
//                    clipPipe(aR, aH, aT, aA, aL);    
//    }    
//}

////aR 扇形角度 
////aH 扇形高度
////aT 管的厚度
////aA 管扇形角度
//module pipeFan(aR, aH, aT, aA) {    
//    difference() {
//        pipe(aR, aH, aT);
//        fan(aA);
//    }
//}
//
////aA [0,180]
//module fan(aA) {
//    aA2 = aA/2;
//    height = 100;    
//    translate([0,0,-2]) union() {
//        rotate([0,0,90 + aA2]) cube([100,100, height]);
//        rotate([0,0,-aA2]) cube([100,100, height]);        
//        rotate([0,0,270]) translate([0,-100,0]) cube([200,200, height]);        
//    }
//}
//
//module pipe(aR, aH, aT) {
//    count = 200;
//    difference() {
//        cylinder(r = aR, h = aH, $fn = count);
//        translate([0,0,-1]) cylinder(r = aR - aT, h = aH + 2, $fn = count);
//    }
//}