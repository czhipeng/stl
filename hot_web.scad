use <hook3.scad>;

radius_out = 90;
g_height = 5;
rate = 0.384;

perA = 360/5;
perB = perA * 3 / 2;

pipe(aR = radius_out, aT = 3, aH = g_height);
pipe(aR = radius_out - 25, aT = 3, aH = g_height);
pipe(aR = radius_out - 50, aT = 3, aH = g_height);


function getX(aA) = (radius_out)*sin(aA);
function getY(aA) = (radius_out)*cos(aA);

difference() {
    star();
    scale([0.9,0.9,1.2]) star();
}
module star() {
    linear_extrude(height = g_height, center = false, convexity = 10) 
    {
        polygon(points = [
        [getX(perA*0), getY(perA*0)],//1
        [getX(perA*2), getY(perA*2)],//3
        [getX(perA*4), getY(perA*4)],//5
        [getX(perA*1), getY(perA*1)],//2
        [getX(perA*3), getY(perA*3)],//4
        ]);

        scale([rate,rate,1]) rotate([0,0,180]) polygon(points = [
        [getX(perA*0), getY(perA*0)],//1
        [getX(perA*1), getY(perA*1)],//2
        [getX(perA*2), getY(perA*2)],//3
        [getX(perA*3), getY(perA*3)],//4
        [getX(perA*4), getY(perA*4)],//5
        ]);
    }
}
