module pipe(aRadius, aThinkness, aHeight) {
    difference(){
        cylinder(h = aHeight, r = aRadius, center = false);
        translate([0,0,-1]) cylinder(h = aHeight + 2, r = aRadius - aThinkness, center = false);
    };
}