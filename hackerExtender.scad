use <hook.scad>;

thinkness = 2;
r_out = 34.22/2;
r_in = r_out - thinkness/2;
mainHeight = 10;
us = 3;

pipe(r_out, mainHeight, thinkness);
translate([0,0, mainHeight - 5]) hookOpm(r_out-thinkness, us * 2.5, 2, 30, 0.3);
