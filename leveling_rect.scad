x = 160;
y = 160;
z = 110;
height = 5;
thinkness = 0.3;

difference() {
    cube([x,y,height],center = true);
    cube([x - 2*thinkness,y - 2*thinkness ,height + 2],center = true);
}
