//aR 扇形角度 
//aH 扇形高度
//aT 管的厚度
//aA 管扇形角度
module pipeFan(aR, aH, aT, aA) {    
    difference() {
        pipe(aR, aH, aT);
        fan(aA);
    }
}

//aA [0,180]
module fan(aA) {
    aA2 = aA/2;
    height = 100;    
    translate([0,0,-2]) union() {
        rotate([0,0,90 + aA2]) cube([100,100, height]);
        rotate([0,0,-aA2]) cube([100,100, height]);        
        rotate([0,0,270]) translate([0,-100,0]) cube([200,200, height]);        
    }
}

module pipe(aR, aH, aT) {
    count = 200;
    difference() {
        cylinder(r = aR, h = aH, $fn = count);
        translate([0,0,-1]) cylinder(r = aR - aT, h = aH + 2, $fn = count);
    }
}

module platform(aU) {
    _platformAngle = 5;
    
    translate([-aU/2,0,aU/2]) {
        rotate([90,0,0]) {
            linear_extrude(height = 4) {
                polygon(points = [
                    [0,0],
                    [aU,aU/tan(90-_platformAngle)],
                    [aU,-(aU+aU/tan(90-_platformAngle))],
                    [0,-aU]
                ]);
            }
        }
    }
}

module left(aU, aA, aR) {
    rotate([0,0,-(aA * (1/3 + 1/15))], center = true) {
        translate([0,aR+0.5,aU*1.5]) {
            platform(aU);
        }
    }
}

module right(aU, aA, aR) {
    mirror([1,0,0]) {left(aU, aA, aR);};
}

module bottomPadding(aR, aH) {
    cylinder(h = aH, r = aR);
}

module hook(
    aU = 3, 
    aA = 40,
    aT = 2,
    aR = 34.22/2 - 2) {
        
    difference() {
        difference() {
            pipeFan(aR, aU*3, aT, aA);
            #left(aU, aA, aR);
            #right(aU, aA, aR);
        };
        translate([0, 0 ,aU * 2]) {
            pipe(aR + 1.6, aU * 1.2, aT);        
        };
        //bottomPadding(aR*2, aU/2);
    }
    
}

hook(aU = 3);
//rotate(21,0,0) translate([0,0,u+0.2]) hook();


